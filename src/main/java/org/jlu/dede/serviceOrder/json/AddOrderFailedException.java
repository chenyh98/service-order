package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class AddOrderFailedException extends RestException {
    public AddOrderFailedException(String message) {
        super(610, message);
        setErrorCode(Integer.valueOf(610));
    }
}
