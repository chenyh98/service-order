package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class NoOrderException extends RestException {
    public NoOrderException(String message) {
        super(623, message);
        setErrorCode(Integer.valueOf(623));
    }
}
