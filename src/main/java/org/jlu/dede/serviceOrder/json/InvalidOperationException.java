package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class InvalidOperationException extends RestException {
    public InvalidOperationException(String message) {
        super(640, message);
        setErrorCode(Integer.valueOf(640));
    }

    public InvalidOperationException() {
        this("Invalid Operation. Same operation may has done before");
    }

}
