package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class NoDriverException extends RestException {
    public NoDriverException(String message) {
        super(HttpStatus.valueOf(620), message);
        setErrorCode(Integer.valueOf(620));
    }

    public NoDriverException() {
        super(620, "the order has no driver");
    }
}
