package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class NoAccountException extends RestException {
    public NoAccountException(String message) {
        super(622, message);
        setErrorCode(Integer.valueOf(622));
    }
}
