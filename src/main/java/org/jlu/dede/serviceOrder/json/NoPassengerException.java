package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;

public class NoPassengerException extends RestException {
    public NoPassengerException(String message) {
        super(621, message);
        setErrorCode(Integer.valueOf(621));
    }

    public NoPassengerException() {
        this("the order has no passenger");
    }

}
