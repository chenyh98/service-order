package org.jlu.dede.serviceOrder.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class TypeErrorException extends RestException {
    public TypeErrorException(String message) {
        super(640, message);
        setErrorCode(Integer.valueOf(640));
    }

    public TypeErrorException() {
        this("type  or status is not available");
    }

}
