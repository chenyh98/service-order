package org.jlu.dede.serviceOrder.feign;

import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "service-location",url = "10.100.0.6:8770")
public interface LocationFeign {

    @RequestMapping(value = "/location/findIdealGoSiteDriver",method = RequestMethod.POST)
    List<String> findIdealGoSiteDriver(@RequestParam String x, @RequestParam String y, @RequestParam Integer id);

}
