package org.jlu.dede.serviceOrder.feign;

import org.jlu.dede.publicUtlis.model.BlackList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface BlacklistFeign {

    @RequestMapping(value = "/blacklists/{id}", method = RequestMethod.GET)
    BlackList getByID(@PathVariable("id") Integer id);

    @RequestMapping(value = "/blacklists/{id}", method = RequestMethod.POST)
    boolean updateBlacklist(@PathVariable("id") Integer id,@RequestBody BlackList blackList);

    @RequestMapping(value = "/blacklists/passengers/{passenger_id}")
    List<BlackList> getByPassenger(@PathVariable("passenger_id") Integer passenger_id);

}
