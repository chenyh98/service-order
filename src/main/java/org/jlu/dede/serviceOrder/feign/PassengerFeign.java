package org.jlu.dede.serviceOrder.feign;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface PassengerFeign {

    @RequestMapping(value = "/passengers/{id}", method = RequestMethod.POST)
    void updatePassenger(@PathVariable("id") Integer id, @RequestBody Passenger passenger);

    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.GET)
    Passenger getByID(@PathVariable("id") Integer id);

    @RequestMapping(value = "/passengers/{id}/order",method = RequestMethod.POST)
    Integer passengerOrder(@PathVariable Integer id, @RequestBody Order order);

    @RequestMapping("/passengers/order/{id}")
    Passenger findByOrder(@PathVariable Integer id);

    @RequestMapping("/accounts/passenger/{id}")
    Account getByPassenger(@PathVariable Integer id);

    @RequestMapping("/orders/passenger/current/{id}")
    Order getCurrentByPassenger(@PathVariable Integer id);

    }
