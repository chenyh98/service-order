package org.jlu.dede.serviceOrder.feign;

import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Push;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "push-model",url="10.100.0.0:8764")
public interface PushFeign {

    @RequestMapping(value = "/passenger/push/AcceptCurrentOrder/{id}", method = RequestMethod.POST)
    boolean pushToPassengerAcceptCurrentOrder(@PathVariable Integer id, @RequestBody Driver driver);

    @RequestMapping(value = "/passenger/push/AcceptBookingOrder/{id}", method = RequestMethod.POST)
    boolean pushToPassengerAcceptBookingOrder(@PathVariable Integer id, @RequestBody Driver driver);

    @RequestMapping(value = "/passenger/push/StartCurrentOrder/{id}", method = RequestMethod.POST)
    boolean pushToPassengerStartCurrentOrder(@PathVariable Integer id, @RequestBody Order order);

    @RequestMapping(value = "/passenger/push/EndCurrentOrder/{id}", method = RequestMethod.POST)
    boolean pushToPassengerEndCurrentOrder(@PathVariable Integer id, @RequestBody Order order);

    @RequestMapping(value = "/driver/push/CurrentOrder", method = RequestMethod.POST)
    boolean pushToDriverCurrentOrder(@RequestParam List<Integer> ids, @RequestBody Order order);

    @RequestMapping(value = "/driver/push/BookingOrder", method = RequestMethod.POST)
    boolean pushToDriverBookingOrder(@RequestParam List<Integer> ids, @RequestBody Order order);

    @RequestMapping(value = "/driver/push/EndCurrentOrder/{id}", method = RequestMethod.POST)
    boolean pushToDriverEndCurrentOrder(@PathVariable Integer id, @RequestBody Order order);

    @RequestMapping(value = "/both/push/CancelCurrentOrder", method = RequestMethod.POST)
    boolean pushToBothCancelCurrentOrder(@RequestBody Order order);

    @RequestMapping(value = "/both/push/CancelBookingOrder", method = RequestMethod.POST)
    boolean pushToBothCancelBookingOrder(@RequestBody Order order);

    @PostMapping("/pushOne/pushMessage")
    boolean pushMessage(@RequestParam String alias, @RequestParam String msgContent, @RequestParam String objType, Object obj);

    @PostMapping("/pushOne/pushAlertAndMessage")
    boolean pushAlertAndMessage(@RequestParam String alias, @RequestParam String notificationTitle, @RequestParam String notificationContent, @RequestParam String msgContent, @RequestParam String objType, @RequestParam Object obj);

    @PostMapping("/pushMany/pushAlertAndMessage")
    boolean pushAlertAndMessage(@RequestParam List<String> alias, @RequestParam String notificationTitle, @RequestParam String notificationContent, @RequestParam String msgContent, @RequestParam String objType, @RequestParam Object obj);

    @PostMapping("/pushOne/pushMessage")
    boolean pushOneMessage(@RequestBody Push apush);



    @PostMapping("/pushMany/pushAlertAndMessage")
    boolean pushManyAlertAndMessage(@RequestBody Push apush);

    //推送通知和自定义消息：根据alias推给个人
    @PostMapping("/pushOne/pushAlertAndMessage")
    public boolean pushOneAlertAndMessage(@RequestBody Push apush);
    @PostMapping("/save/pullCurrentOrder/{id}")
    public boolean savePullCurrentOrder(@RequestBody List<String> driverList,@PathVariable("id") Integer id);
    @PostMapping("/save/pullBookingOrder/{id}")
    public boolean savePullBookingOrder(@RequestBody List<String> driverList,@PathVariable("id") Integer id);

    @PostMapping("/pushAll")
    boolean pushAll(@RequestBody Push apush);

}
