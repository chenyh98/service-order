package org.jlu.dede.serviceOrder.controller;


import org.jlu.dede.serviceOrder.Service.CommonOrderService;
import org.jlu.dede.serviceOrder.Service.PassengerOrderService;
import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order/passenger")
public class PassengerOrderController {

    @Autowired
    private CommonOrderService commonOrderService;

    @Autowired
    private PassengerOrderService passengerOrderService;

    @PostMapping("/addCurrentOrder/{x}/{y}")
    public Integer addCurrentOrder(@RequestBody Order order, @PathVariable String x, @PathVariable String y) {
        return commonOrderService.addCurrentOrder(order, x, y);
    }
    @PostMapping("/addBookingOrder/{x}/{y}")
    public Integer addBookingOrder(@RequestBody Order order, @PathVariable String x, @PathVariable String y) {
        return  commonOrderService.addBookingOrder(order, x, y);
    }

    /*
    @PostMapping("/addCurrentOrder")
    public Integer addCurrentOrder(@RequestBody Order order) {
        return commonOrderService.addCurrentOrder(order);
    }

    @PostMapping("/addBookingOrder")
    public Integer addBookingOrder(@RequestBody Order order) {
        return  commonOrderService.addBookingOrder(order);
    }
    */
    @RequestMapping("/queryCurrentOrder")
    public Order queryCurrentOrder(@RequestParam("id") Integer id) {
        return passengerOrderService.returnCurrentOrder(id);
    }

    @RequestMapping("/queryBookingOrder")
    public List<Order> queryBookingOrder(@RequestParam("id") Integer id) {
        return passengerOrderService.returnBookingOrder(id);
    }

    @RequestMapping("/cancelCurrentOrder")
    public boolean cancelCurrentOrder(@RequestParam("id") Integer id) {
        return passengerOrderService.cancelCurrentOrder(id);
    }

    @RequestMapping("/cancelBookingOrder")
    public boolean cancelBookingOrder(@RequestBody Order order) {
        return passengerOrderService.cancelBookingOrder(order);
    }

    @RequestMapping("/arriveDestination")
    public boolean arriveDestination(@RequestParam("id") Integer id) {
        return passengerOrderService.arriveDestination(id);
    }

    @RequestMapping("/queryHistoryOrder")
    public List<Order> queryHistoryOrder(@RequestParam("id") Integer id) {
        return passengerOrderService.queryHistoryOrder(id);
    }

    @RequestMapping("/addEvaluation")
    public boolean addEvaluation(@RequestBody Order order) {
        return passengerOrderService.addEvaluation(order);
    }

}
