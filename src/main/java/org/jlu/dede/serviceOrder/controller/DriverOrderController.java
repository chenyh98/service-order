package org.jlu.dede.serviceOrder.controller;

import org.jlu.dede.serviceOrder.Service.DriverOrderService;
import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order/driver")
public class DriverOrderController {

    @Autowired
    private DriverOrderService driverOrderService;

    @PostMapping("/acceptCurrentOrder")
    public boolean acceptCurrentOrder(@RequestBody Order order) {
        return driverOrderService.acceptCurrentOrder(order);
    }

    @PostMapping("/acceptBookingOrder")
    public Order acceptBookingOrder(@RequestBody Order order) {
        return driverOrderService.acceptBookingOrder(order);
    }

    @PostMapping("/startDriving")
    public boolean startDriving(@RequestParam("id") Integer id) {
        return driverOrderService.startDriving(id);
    }

    @RequestMapping("/cancelCurrentOrder")
    public boolean cancelCurrentOrder(@RequestParam("id") Integer id) {
        return driverOrderService.cancelCurrentOrder(id);
    }
    @PostMapping("/cancelBookingOrder")
    public boolean cancelBookingOrder(@RequestBody Order order) {
        return driverOrderService.cancelBookingOrder(order);
    }

    @RequestMapping("/queryCurrentOrder")
    public Order queryCurrentOrder(@RequestParam("id") Integer id) {
        return driverOrderService.returnCurrentOrder(id);
    }

    @RequestMapping("/queryBookingOrder")
    public List<Order> queryBookingOrder(@RequestParam("id") Integer id) {
        return driverOrderService.returnBookingOrder(id);
    }

    @RequestMapping("/arriveDestination")
    public boolean arriveDestination(@RequestParam("id") Integer id) {
        return driverOrderService.arriveDestination(id);
    }

    @RequestMapping("/queryHistoryOrder")
    public List<Order> queryHistoryOrder(@RequestParam("id") Integer id) {
        return driverOrderService.queryHistoryOrder(id);
    }

}
