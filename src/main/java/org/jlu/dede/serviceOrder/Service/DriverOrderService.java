package org.jlu.dede.serviceOrder.Service;

import org.jlu.dede.publicUtlis.model.Order;

import java.util.List;

public interface DriverOrderService {
    boolean acceptCurrentOrder(Order order);
    Order acceptBookingOrder(Order order);
    boolean cancelCurrentOrder(Integer id);
    boolean cancelBookingOrder(Order order);
    Order returnCurrentOrder(Integer id);
    List<Order> returnBookingOrder(Integer id);
    boolean arriveDestination(Integer id);
    boolean startDriving(Integer id);
    List<Order> queryHistoryOrder(Integer id);
    boolean judgeCreditValue(Integer driverId);
}
