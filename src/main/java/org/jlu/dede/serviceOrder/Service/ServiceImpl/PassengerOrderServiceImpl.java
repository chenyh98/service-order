package org.jlu.dede.serviceOrder.Service.ServiceImpl;

import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.serviceOrder.feign.*;
import org.jlu.dede.serviceOrder.json.InvalidOperationException;
import org.jlu.dede.serviceOrder.json.NoDriverException;
import org.jlu.dede.serviceOrder.json.NoOrderException;
import org.jlu.dede.serviceOrder.json.NoPassengerException;
import org.jlu.dede.serviceOrder.Service.PassengerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class PassengerOrderServiceImpl implements PassengerOrderService {

    @Autowired
    private BlacklistFeign blacklistFeign;
    
    @Autowired
    private OrderFeign orderFeign;
    
    @Autowired
    private PassengerFeign passengerFeign;

    @Autowired
    private DriverFeign driverFeign;

    @Autowired
    private PushFeign pushFeign;

    @Autowired
    private StringRedisTemplate srt;


    public  Order returnCurrentOrder(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        Order order = passengerFeign.getCurrentByPassenger(passenger.getId());
        return order;
    }
    public  List<Order> returnBookingOrder(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        List<Order> orderList = orderFeign.getByPassenger(passenger.getId());
        Iterator<Order> iterator = orderList.iterator();
        while(iterator.hasNext()) {
            Order order = iterator.next();
            if(order.getType() != 3 || order.getState() >= 2) {
                iterator.remove();
            }
        }
        return orderList;
    }
    public  boolean cancelCurrentOrder(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        Order order = passengerFeign.getCurrentByPassenger(passenger.getId());
        if(order != null) {
            if(order.getState() == -1 || order.getState() >= 2)
                throw new InvalidOperationException();
            order.setState(-1);
            passengerFeign.passengerOrder(passenger.getId(), order);
            Driver driver = driverFeign.findByOrder(order.getId());
            if(driver != null)
                driverFeign.driverOrder(driver.getId(), order);
            orderFeign.updateOrder(order.getId(), order);
           // srt.delete("CurrentOrder");
            //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order canceled", "You have canceled the current order","", "Order", order);
            //pushFeign.pushAlertAndMessage("d"+driver.getId(), "order canceled", "Current order has been canceled by the passenger","", "Order", order);
            Push p_push = new Push("p"+passenger.getId(), "order canceled", "You have canceled the current order","", "Order", order);
            Push d_push = new Push("d"+driver.getId(), "order canceled", "Current order has been canceled by the passenger","", "Order", order);
            pushFeign.pushOneAlertAndMessage(p_push);
            pushFeign.pushOneAlertAndMessage(d_push);
        }
        else
            throw new NoOrderException("the passenger has no current order");
        //pushFeign.pushToBothCancelCurrentOrder(order);

        return true;
    }
    public  boolean cancelBookingOrder(Order order) {
        if(order.getState() == -1 || order.getState() >= 2)
            throw new InvalidOperationException();
        order.setState(-1);
        orderFeign.updateOrder(order.getId(), order);
        //srt.delete("BookingOrder");
        //pushFeign.pushToBothCancelBookingOrder(order);
        Driver driver = driverFeign.findByOrder(order.getId());
        Passenger passenger = passengerFeign.findByOrder(order.getId());
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        if(driver == null)
            throw new NoDriverException();
        //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order canceled", "You have canceled the booking order","", "Order", order);
        //pushFeign.pushAlertAndMessage("d"+driver.getId(), "order canceled", "Booking order has been canceled by the passenger","", "Order", order);
        Push p_push = new Push("p"+passenger.getId(), "order canceled", "You have canceled the booking order","", "Order", order);
        Push d_push = new Push("d"+driver.getId(), "order canceled", "Booking order has been canceled by the passenger","", "Order", order);
        pushFeign.pushOneAlertAndMessage(p_push);
        pushFeign.pushOneAlertAndMessage(d_push);

        return true;
    }
    public  boolean judgeBlackList(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        if(passenger == null)
            throw new NoPassengerException("no passenger found");
        Order order = passengerFeign.getCurrentByPassenger(passenger.getId());
        if(order == null)
            throw new NoOrderException("the passenger has no current order");
        Driver driver = driverFeign.findByOrder(order.getId());
        if(driver == null)
            throw new NoDriverException("the order has no driver");
        List<BlackList> blackLists = blacklistFeign.getByPassenger(passenger.getId());
        for(BlackList blackList : blackLists) {
            if(blackList.getDriver().getId().equals(driver.getId()))
                return true;
        }
        return false;
    }
    public  boolean arriveDestination(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        if(passenger == null)
            throw new NoPassengerException("no passenger found");
        Order order = passengerFeign.getCurrentByPassenger(passenger.getId());
        if(order == null)
            throw new NoOrderException("the passenger has no current order");
        if(order.getState() == 3)
            throw new InvalidOperationException();
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        order.setArriveDestinationTime(time);
        order.setState(3);
        Driver driver = driverFeign.findByOrder(order.getId());
        if(driver == null)
            throw new NoDriverException("the order has no driver");
        order.setDriver(driver);
        order.setPassenger(passenger);
        orderFeign.updateOrder(order.getId(), order);
        driverFeign.driverOrder(driver.getId(), order);
        //pushFeign.pushToDriverEndCurrentOrder(driver.getId(), order);
        //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order's status changed", "You have arrived the destination","", "Order", order);
        Push push = new Push("p"+passenger.getId(), "order's status changed", "You have arrived the destination","", "Order", order);
        pushFeign.pushOneAlertAndMessage(push);
        return true;
    }
    public  List<Order> queryHistoryOrder(Integer id) {
        Passenger passenger = passengerFeign.getByID(id);
        if(passenger == null)
            throw new NoPassengerException("no passenger found");
        List<Order> allOrders = orderFeign.getByPassenger(passenger.getId());
        Iterator<Order> iterator = allOrders.iterator();
        while(iterator.hasNext()) {
            Order order = iterator.next();
            if(order.getState() <= 2) {
                iterator.remove();
            }
        }
        return allOrders;
    }
    public  boolean addEvaluation(Order order) {
        orderFeign.updateOrder(order.getId(), order);
        return true;
    }
    public boolean judgeOutstandingOrder(Integer userId) {
        Passenger passenger = passengerFeign.getByID(userId);
        if(passenger == null)
            throw new NoPassengerException("no passenger found");
        List<Order> orderList = orderFeign.getByPassenger(passenger.getId());
        for(Order order : orderList) {
            if(order.getState() == 3)
                return true;
        }
        return false;
    }
}
