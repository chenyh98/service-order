package org.jlu.dede.serviceOrder.Service.ServiceImpl;

import org.jlu.dede.publicUtlis.model.Push;
import org.jlu.dede.serviceOrder.feign.DriverFeign;
import org.jlu.dede.serviceOrder.feign.OrderFeign;
import org.jlu.dede.serviceOrder.feign.PassengerFeign;
import org.jlu.dede.serviceOrder.feign.PushFeign;
import org.jlu.dede.serviceOrder.json.*;
import org.jlu.dede.serviceOrder.Service.DriverOrderService;
import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class DriverOrderServiceImpl implements DriverOrderService {

    @Autowired
    private DriverFeign driverFeign;

    @Autowired
    private OrderFeign orderFeign;

    @Autowired
    private PassengerFeign passengerFeign;

    @Autowired
    private PushFeign pushFeign;



    public  boolean acceptCurrentOrder(Order currentOrder){
        if(currentOrder.getState() != 1 || (currentOrder.getType() != 1 && currentOrder.getType() != 2))
            throw new TypeErrorException();
        Driver driver = currentOrder.getDriver();
        Passenger passenger = passengerFeign.findByOrder(currentOrder.getId());
        if(driver == null)
            throw new NoDriverException();
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        if(driverFeign.getCurrentByDriver(driver.getId()) != null)
            throw new AddOrderFailedException("The driver has received a order before");
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        currentOrder = orderFeign.getByID(currentOrder.getId());
        currentOrder.setAcceptTime(time);
        currentOrder.setDriver(null);
        currentOrder.setPassenger(null);
        driver.setOrder(currentOrder);
        driverFeign.updateDriver(driver.getId(), driver);

        //pushFeign.pushToPassengerAcceptCurrentOrder(passenger.getId(), driver);
        //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order's status changed", "order has been accepted","", "Order", currentOrder);
        //pushFeign.pushMessage("p"+passenger.getId(), "order's status changed", "Order", currentOrder);
        //Push push = new Push("p"+passenger.getId(), "通知", "有司机接受您的订单了！", "order's status changed", "Order", currentOrder);

        //currentOrder.setDriver(driver);
        //currentOrder.setPassenger(passenger);
        Order newCurrentOrder = orderFeign.getByID(currentOrder.getId());
        driver.setOrder(null);
        newCurrentOrder.setDriver(driver);
        passenger.setOrder(null);
        newCurrentOrder.setPassenger(passenger);
        newCurrentOrder.setState(1);
        newCurrentOrder.setType(currentOrder.getType());
        newCurrentOrder.setAcceptTime(time);
        newCurrentOrder.setCreateTime(currentOrder.getCreateTime());
        newCurrentOrder.setIdealDepartSite(currentOrder.getIdealDepartSite());
        newCurrentOrder.setIdealDestinationSite(currentOrder.getIdealDestinationSite());
        newCurrentOrder.setStartTime(currentOrder.getStartTime());
        System.out.println("before push");
        Push push = new Push("p"+passenger.getId(), "", "Order", newCurrentOrder);
        pushFeign.pushAll(push);
        System.out.println("after push");
        orderFeign.updateOrder(currentOrder.getId(), newCurrentOrder);
        //srt.delete("CurrentOrder");
        return true;
    }
    public Order acceptBookingOrder(Order bookingOrder) {
        if(bookingOrder.getState() != 1 || bookingOrder.getType() != 3)
            throw new TypeErrorException();
        orderFeign.updateOrder(bookingOrder.getId(), bookingOrder);

        Passenger passenger = passengerFeign.findByOrder(bookingOrder.getId());
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        Driver driver = bookingOrder.getDriver();
        if(driver == null)
            throw new NoDriverException();
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        bookingOrder.setAcceptTime(time);
        //Push push = new Push("p"+passenger.getId(), "order's status changed", "order has been accepted","", "Order", bookingOrder);
        Push push = new Push("p"+passenger.getId(), "", "Order", bookingOrder);
        pushFeign.pushAll(push);
        //srt.delete("BookingOrder");
        return bookingOrder;
    }
    public boolean cancelCurrentOrder(Integer id) {
        Driver driver = driverFeign.getByID(id);
        Order order = driverFeign.getCurrentByDriver(driver.getId());
        if(order != null) {
            if(order.getState() == -1 || order.getState() >= 2)
                throw new InvalidOperationException();
            order.setState(-1);
            order.setDriver(null);
            driverFeign.driverOrder(driver.getId(), order);
            Passenger passenger = passengerFeign.findByOrder(order.getId());
            if(passenger != null)
                passengerFeign.passengerOrder(passenger.getId(), order);
            order.setDriver(driver);
            orderFeign.updateOrder(order.getId(), order);

            //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order canceled", "Current order has been accepted by the driver","", "Order", order);
            //pushFeign.pushAlertAndMessage("d"+driver.getId(), "order canceled", "you have canceled the current order","", "Order", order);
            //Push p_push = new Push("p"+passenger.getId(), "order canceled", "Current order has been accepted by the driver","", "Order", order);
            //Push d_push = new Push("d"+driver.getId(), "order canceled", "you have canceled the current order","", "Order", order);
            Push p_push = new Push("p"+passenger.getId(), "", "Order", order);
            Push d_push = new Push("d"+driver.getId(), "", "Order", order);
            pushFeign.pushAll(p_push);
            pushFeign.pushAll(d_push);
        }
        else
            throw new NoOrderException("the driver has no current order");
        //srt.opsForValue().set("CurrentOrder","C"+order.getId().toString());
        return true;
    }
    public boolean cancelBookingOrder(Order order) {
        if(order.getState() == -1 || order.getState() >= 2)
            throw new InvalidOperationException();
        order.setState(-1);
        orderFeign.updateOrder(order.getId(), order);
        Driver driver = driverFeign.findByOrder(order.getId());
        Passenger passenger = passengerFeign.findByOrder(order.getId());
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        if(driver == null)
            throw new NoDriverException();

        //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order canceled", "Booking order has been accepted by the driver","", "Order", order);
        //pushFeign.pushAlertAndMessage("d"+driver.getId(), "order canceled", "you have canceled the booking order","", "Order", order);
        Push p_push = new Push("p"+passenger.getId(), "order canceled", "Current order has been accepted by the driver","", "Order", order);
        Push d_push = new Push("d"+driver.getId(), "order canceled", "you have canceled the current order","", "Order", order);
        pushFeign.pushOneAlertAndMessage(p_push);
        pushFeign.pushOneAlertAndMessage(d_push);
        //srt.opsForValue().set("BookingOrder",order.getId().toString());
        return true;
    }
    public  Order returnCurrentOrder(Integer id) {
        Driver driver = driverFeign.getByID(id);
        return driverFeign.getCurrentByDriver(driver.getId());
    }
    public  List<Order> returnBookingOrder(Integer id) {
        Driver driver = driverFeign.getByID(id);
        List<Order> orderList = orderFeign.getByDriver(driver.getId());
        Iterator<Order> iterator = orderList.iterator();
        while (iterator.hasNext()) {
            Order temp = iterator.next();
            if(temp.getType() != 3 || temp.getState() >= 2) {
                iterator.remove();
            }
        }
        return orderList;
    }
    public boolean startDriving(Integer id) {
        Driver driver = driverFeign.getByID(id);
        Order order = driverFeign.getCurrentByDriver(driver.getId());
        if(order == null)
            throw new NoOrderException("the driver has no current order");
        if(order.getState() == 2)
            throw new InvalidOperationException();
        Passenger passenger = passengerFeign.findByOrder(order.getId());
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        order.setStartTime(time);
        order.setState(2);
        orderFeign.updateOrder(order.getId(), order);
        //pushFeign.pushToPassengerStartCurrentOrder(passenger.getId(), order);
        //pushFeign.pushAlertAndMessage("p"+passenger.getId(), "order's status changed", "The trip has begun","", "Order", order);
        //Push push = new Push("p"+passenger.getId(), "order's status changed", "The trip has begun","", "Order", order);
        Push push = new Push("p"+passenger.getId(), "", "Order", order);
        pushFeign.pushAll(push);
        return true;
    }
    /*
    public boolean arriveStartPlace(Order order) {
    }
    */
    public boolean arriveDestination(Integer id) {
        Driver driver = driverFeign.getByID(id);
        Order order = driverFeign.getCurrentByDriver(driver.getId());
        if(order == null)
            throw new NoOrderException("the driver has no current order");
        if(order.getState() == 3)
            throw new InvalidOperationException();
        order.setState(3);
        Passenger passenger = passengerFeign.findByOrder(order.getId());
        if(passenger == null)
            throw new NoPassengerException("the order has no passenger");
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        order.setArriveDestinationTime(time);
        order.setPassenger(passenger);
        order.setDriver(driver);
        driverFeign.driverOrder(driver.getId(), order);
        driver.setOrder(null);
        order.setDriver(driver);
        orderFeign.updateOrder(order.getId(), order);
        //pushFeign.pushAlertAndMessage("d"+driver.getId(), "order's status changed", "The passenger has clicked the finish","", "Order", order);
        //pushFeign.pushToPassengerEndCurrentOrder(passenger.getId(), order);
        //Push push = new Push("d"+driver.getId(), "order's status changed", "The passenger has clicked the finish","", "Order", order);
        Push push = new Push("p"+passenger.getId(), "", "Order", order);
        pushFeign.pushAll(push);
        return true;
    }
    public List<Order> queryHistoryOrder(Integer id) {
        Driver driver = driverFeign.getByID(id);
        List<Order> allOrders = orderFeign.getByDriver(driver.getId());
        Iterator<Order> iterator =allOrders.iterator();
        while (iterator.hasNext()) {
            Order order = iterator.next();
            if(order.getState() <= 2) {
                iterator.remove();
            }
        }
        return  allOrders;
    }
    public boolean judgeCreditValue(Integer driverId) {
        Driver driver = driverFeign.getByID(driverId);
        if(driver == null)
            throw new NoDriverException("no driver found");
        return driver.getAccount().getVerifyValue() > 80;
    }
}
