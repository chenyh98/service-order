package org.jlu.dede.serviceOrder.Service;


import org.jlu.dede.publicUtlis.model.Order;

public interface CommonOrderService {
    Integer addCurrentOrder(Order order);
    Integer addCurrentOrder(Order order, String x, String y);
    Integer addBookingOrder(Order order);
    Integer addBookingOrder(Order order, String x, String y);
    boolean punishDriver(Order order, double verifyValue);
    boolean punishPassenger(Order order, double verifyValue);
}
