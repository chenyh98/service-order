package org.jlu.dede.serviceOrder.Service;

import org.jlu.dede.publicUtlis.model.Order;
import java.util.List;

public interface PassengerOrderService {
    Order returnCurrentOrder(Integer id);
    List<Order> returnBookingOrder(Integer id);
    boolean cancelCurrentOrder(Integer id);
    boolean cancelBookingOrder(Order order);
    boolean judgeBlackList(Integer id);
    boolean arriveDestination(Integer id);
    List<Order> queryHistoryOrder(Integer id);
    boolean addEvaluation(Order order);
    boolean judgeOutstandingOrder(Integer userId);
}
