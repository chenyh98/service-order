package org.jlu.dede.serviceOrder.Service.ServiceImpl;

import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.serviceOrder.feign.*;
import org.jlu.dede.serviceOrder.json.*;
import org.jlu.dede.serviceOrder.Service.CommonOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.SortParameters;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

@Service
public class CommonOrderServiceImpl implements CommonOrderService {

    @Autowired
    private AccountFeign accountFeign;

    @Autowired
    private OrderFeign orderFeign;

    @Autowired
    private PassengerFeign passengerFeign;

    @Autowired
    private LocationFeign locationFeign;

    @Autowired
    private PushFeign pushFeign;

    @Autowired
    private DriverFeign driverFeign;




    public Integer addCurrentOrder(Order order, String x, String y) {

        if(order.getType() != 1 && order.getType() != 2)
            throw new TypeErrorException();
        Passenger passenger = order.getPassenger();
        if(passenger == null)
            throw new NoPassengerException();
        if(passengerFeign.getCurrentByPassenger(passenger.getId()) != null)
            throw new AddOrderFailedException("passenger has a current order");
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        order.setCreateTime(time);
        passenger.setOrder(order);
        Integer id = passengerFeign.passengerOrder(passenger.getId(), order);
        //srt.opsForValue().set("a","1");
        System.out.println(id);
        //srt.opsForValue().set("CurrnetOrder",id.toString());

        List<String> driverList = locationFeign.findIdealGoSiteDriver(x, y, id);
        pushFeign.savePullCurrentOrder(driverList,id);
        Iterator<String> iterator = driverList.iterator();
        System.out.println("begin while");
        while (iterator.hasNext()) {
            String sid = iterator.next();
            Integer iid = Integer.parseInt(sid.substring(1));
            System.out.println(iid);
            Driver driver = driverFeign.getByID(iid);
            if(driver != null) {
                if((driver.getState() == 1 && order.getType() != 1) || (driver.getState() == 2 && order.getType() != 2))
                    iterator.remove();
//                int rand=rd.nextInt(100);
//                srt.opsForValue().set("CurrentOrder"+iid.toString()+rand,id.toString());
//                System.out.println(srt.opsForValue().get("CurrentOrder"+iid.toString()+rand));
            }
        }
       // int rand=rd.nextInt(100);
        //srt.opsForValue().set("CurrentOrder33"+rand,id.toString());
        //System.out.println(srt.opsForValue().get("CurrentOrder33"+rand));
        //pushFeign.pushToDriverCurrentOrder(driverList, order);
        //pushFeign.pushAlertAndMessage(driverList, "new order found!", "You have just received a new order", "You have just received a new order", "Order", order);
//        Push push = new Push(driverList, "new order found!", "You have just received a new order", "You have just received a new order", "Order", order);
//        pushFeign.pushManyAlertAndMessage(push);

        return id;
    }
    public Integer addCurrentOrder(Order order) {

       if(order.getType() != 0)
           throw new TypeErrorException();
       Passenger passenger = passengerFeign.findByOrder(order.getId());
       // srt.opsForValue().set("CurrnetOrder",order.getId().toString());
       if(passenger == null)
           throw new NoPassengerException();
       if(passenger.getOrder() != null) {
            throw new AddOrderFailedException("passenger has a current order");
       }
       passenger.setOrder(order);

       return passengerFeign.passengerOrder(passenger.getId(), order);
   }
    public Integer addBookingOrder(Order order, String x, String y) {
        if(order.getType() != 3)
            throw new TypeErrorException();
        Date nowDate  = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(nowDate);
        order.setCreateTime(time);
        order.setCreateTime(time);
        Integer id = orderFeign.addOrder(order);

        Passenger passenger = order.getPassenger();
        if(passenger == null)
            throw new NoPassengerException();
        if(passenger.getOrder() != null) {
            throw new AddOrderFailedException("passenger has a current order");
        }
        List<String> driverList = locationFeign.findIdealGoSiteDriver(x, y, order.getId());
        pushFeign.savePullBookingOrder(driverList,id);
        Iterator<String> iterator = driverList.iterator();
        while (iterator.hasNext()) {
            String sid = iterator.next();
            Integer iid = Integer.parseInt(sid.substring(1));
            Driver driver = driverFeign.getByID(iid);
            if(driver.getState() == 1)
                iterator.remove();
//            int rand=rd.nextInt(100);
//            srt.opsForValue().set("BookingOrder"+iid.toString()+rand,id.toString());
        }
        //pushFeign.pushToDriverCurrentOrder(driverList, order);
        //pushFeign.pushAlertAndMessage(driverList, "new order found!", "You have just received a new specific car type order", "", "Order", order);
//        Push push =new Push(driverList, "new order found!", "You have just received a new specific car type order", "", "Order", order);
//        pushFeign.pushManyAlertAndMessage(push);

        return id;
    }
   public Integer addBookingOrder(Order order) {
       if(order.getType() != 1)
           throw new TypeErrorException();
       //srt.opsForValue().set("BookingOrder",order.getId().toString());
       Integer id = orderFeign.addOrder(order);
       Passenger passenger = passengerFeign.findByOrder(order.getId());
       if(passenger == null)
           throw new NoPassengerException();

       return id;
   }
   public boolean punishDriver(Order order, double verifyValue) {
       Driver driver = driverFeign.findByOrder(order.getId());
       if(driver == null)
           throw new NoDriverException("the order has no driver");
       Account driverAccount = driverFeign.getByDriver(driver.getId());
       if(driverAccount == null)
           throw new NoAccountException("the driver has no account");
       return punish(driverAccount, verifyValue);
   }
   public boolean punishPassenger(Order order, double verifyValue) {
       Passenger passenger = passengerFeign.findByOrder(order.getId());
       if(passenger == null)
           throw new NoPassengerException("the order has no passenger");
       Account passengerAccount = passengerFeign.getByPassenger(passenger.getId());
       if(passengerAccount == null)
           throw new NoAccountException("the passenger has no account");
       return punish(passengerAccount, verifyValue);
   }
   private boolean punish(Account account, double verifyValue) {
       double currentValue = account.getVerifyValue();
       account.setVerifyValue(currentValue - verifyValue);
       accountFeign.updateAccounts(account.getId(), account);
       return true;
   }

}
