package org.jlu.dede.serviceOrder.dao;

import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Integer> {
}
