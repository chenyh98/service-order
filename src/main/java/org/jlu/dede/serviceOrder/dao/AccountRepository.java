package org.jlu.dede.serviceOrder.dao;

import org.jlu.dede.publicUtlis.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
}
