package org.jlu.dede.serviceOrder.dao;

import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findByDriver(Driver driver);
    List<Order> findByPassenger(Passenger passenger);
}
