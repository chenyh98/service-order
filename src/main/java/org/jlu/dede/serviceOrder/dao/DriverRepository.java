package org.jlu.dede.serviceOrder.dao;

import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<Driver,Integer> {
}
