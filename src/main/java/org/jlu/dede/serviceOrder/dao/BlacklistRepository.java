package org.jlu.dede.serviceOrder.dao;

import org.jlu.dede.publicUtlis.model.BlackList;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlacklistRepository extends JpaRepository<BlackList, Integer> {
    List<BlackList> findByPassenger(Passenger passenger);
}
